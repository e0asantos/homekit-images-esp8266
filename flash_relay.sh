#!/bin/bash

POWERON_STATE="ON" # Change this to "OFF" if you want that your Sonoff will be OFF when powered 

SONOFF_PORT="/dev/tty.usbserial-A800K5UT"
# algunos modulos requieres flash_mode dio y otros qio, dio es para 2 pines de comunicacion y qio usa 4,
# lo que quiere decir que al usar DIO puede liberar 2 pines como en el esp12
#  tenia Dout originalmente y lo sustityo por DIO para ver si el calor ayuda
esptool.py \
            -p $SONOFF_PORT \
            --baud 115200 \
            write_flash \
            -fs 2MB \
            -fm dio \
            -ff 40m \
            0x0 ./firmware/rboot.bin \
            0x1000 ./firmware/blank_config.bin \
            0x2000 ./firmware/suitch_relay_serialized2.bin
