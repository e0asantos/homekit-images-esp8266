#!/bin/bash

POWERON_STATE="ON" # Change this to "OFF" if you want that your Sonoff will be OFF when powered 

SONOFF_PORT="/dev/tty.usbserial-14110"
# algunos modulos requieres flash_mode dio y otros qio, dio es para 2 pines de comunicacion y qio usa 4,
# lo que quiere decir que al usar DIO puede liberar 2 pines como en el esp12
esptool.py \
            -p $SONOFF_PORT \
            --baud 115200 \
            write_flash \
            --flash_size 4MB \
            --flash_mode dio \
            --flash_freq 40m \
            0x0 ./firmware/rboot.bin \
            0x1000 ./firmware/blank_config.bin \
            0x2000 ./firmware/esp-ir-suitch-temp.bin
