/*
example https://github.com/SuperHouse/esp-open-rtos/blob/a8c60e096093e9e9f4a60b885676adc2cf5b790a/extras/pwm/pwm.c
documentation
https://github.com/jcard0na/pwm_test_esp8266/blob/master/pwm_test.c
https://github.com/itmarshall/esp8266-projects/tree/master/servo
http://smallbits.marshall-tribe.net/blog/2018/01/21/esp8266-move-servo
 * Example of using esp-wifi-config library to join
 * accessories to WiFi networks.
 *
 * When accessory starts without WiFi config or
 * configure WiFi network is not available, it creates
 * it's own WiFi AP "my-accessory-XXXXXX" (where XXXXXX
 * is last 3 digits of accessory's mac address in HEX).
 * If you join that network, you will be presented with
 * a page to choose WiFi network to connect accessory to.
 *
 * After successful connection accessory shuts down AP.
 *
 */

#include <stdio.h>
#include "espressif/esp_common.h"
#include <esp/uart.h>
#include <esp8266.h>
#include <FreeRTOS.h>
#include <task.h>
#include <homekit/homekit.h>
#include <homekit/characteristics.h>
#include <wifi_config.h>
#include <pwm.h>
#define PWM_MIN 544    // 1ms
#define PWM_MAX 1500    // 2ms

const int led_gpio = 2;
const int servo_pin = 5;
uint32_t bri = 0;
uint32_t previous_value = 0;
bool on;
uint8_t pins[1];
int steps_left = 0;
bool direction = true;
int step_sequence = 0;
int stepsModel [ 8 ][ 4 ] =
{ {1, 0, 0, 0},
  {1, 1, 0, 0},
  {0, 1, 0, 0},
  {0, 1, 1, 0},
  {0, 0, 1, 0},
  {0, 0, 1, 1},
  {0, 0, 0, 1},
  {1, 0, 0, 1}
};
void led_write(bool on) {
    gpio_write(led_gpio, on ? 0 : 1);
    // bri = 18;
    // servoSET();
    // vTaskDelay(1200 / portTICK_PERIOD_MS);
    // bri = 50;
    // servoSET();
    int w = previous_value;
    if (on){
      //we identify if the new value is less in which case we need to change direction
      direction = false;
    } else {
      direction = true;
    }
   // printf("steps to make [%5d]\n", w);
    for(int steps_to_make = 0; steps_to_make < w; steps_to_make++){
       move_stepper();
       vTaskDelay(10 / portTICK_PERIOD_MS);
    }
}

void led_on_callback(homekit_characteristic_t *_ch, homekit_value_t on, void *context);

homekit_characteristic_t led_on = HOMEKIT_CHARACTERISTIC_(
    ON, false, .callback=HOMEKIT_CHARACTERISTIC_CALLBACK(led_on_callback)
);

void led_init() {
    gpio_enable(led_gpio, GPIO_OUTPUT);
    led_write(led_on.value.bool_value);
    pins[0] = servo_pin;
    pwm_init(1, pins, false);
    printf("pwm_set_freq(50) \n");
    pwm_set_freq(50);
    printf("pwm_set_duty(33333)   \n");
    pwm_set_duty(1500);
    pwm_start();
}
void stepper_init(){
/** the stepper motor has 4 wires connected to GPIO 16,14,12,13 **/
   gpio_enable(16, GPIO_OUTPUT);
   gpio_enable(14, GPIO_OUTPUT);
   gpio_enable(12, GPIO_OUTPUT);
   gpio_enable(13, GPIO_OUTPUT);
}
void setDirection()
{
  if (direction)
    step_sequence++;
  else
    step_sequence--;

  step_sequence = ( step_sequence + 8 ) % 8 ;
}
void move_stepper()            //Avanza un paso
{
  gpio_write( 16, stepsModel[step_sequence][0] );
  gpio_write( 14, stepsModel[step_sequence][1] );
  gpio_write( 12, stepsModel[step_sequence][2] );
  gpio_write( 13, stepsModel[step_sequence][3] );

  setDirection();
}


void led_on_callback(homekit_characteristic_t *_ch, homekit_value_t on, void *context) {
    led_write(led_on.value.bool_value);
}

void led_identify_task(void *_args) {
    for (int i=0; i<3; i++) {
        for (int j=0; j<2; j++) {
            led_write(true);
            vTaskDelay(100 / portTICK_PERIOD_MS);
            led_write(false);
            vTaskDelay(100 / portTICK_PERIOD_MS);
        }

        vTaskDelay(250 / portTICK_PERIOD_MS);
    }

    led_write(led_on.value.bool_value);

    vTaskDelete(NULL);
}

void led_identify(homekit_value_t _value) {
    printf("LED identify\n");
    xTaskCreate(led_identify_task, "LED identify", 128, NULL, 2, NULL);
}

void servoSET_task(void *pvParameters) {
    int w;
    //we use a constant -6- with these configuratio of the PWM
    w =  ((uint32_t)(bri *6 ) * (PWM_MAX - PWM_MIN) / 180) + PWM_MIN;
    pwm_set_duty(w);
    printf("ON  %3d [%5d]\n", (int)bri , w);
    pwm_start();
    vTaskDelete(NULL);
}
void stepperMotorSET_task(void *pvParameters) {
    int w;
    if ( previous_value > (bri*5) ){
      //we identify if the new value is less in which case we need to change direction
      direction = false;
      w = previous_value - (bri*5);
    } else {
      direction = true;
      w = (bri*5) - previous_value;
    }
   // printf("steps to make [%5d]\n", w);
    for(int steps_to_make = 0; steps_to_make < w; steps_to_make++){
       move_stepper();
       vTaskDelay(10 / portTICK_PERIOD_MS);
    }
    previous_value = bri*5;
    vTaskDelete(NULL);
}


void servoSET() {
    xTaskCreate(servoSET_task, "servo Set", 256, NULL, 2, NULL);
}
void stepperMotorSET() {
    xTaskCreate(stepperMotorSET_task, "stepper Set", 256, NULL, 2, NULL);
}
homekit_value_t servo_get() { return HOMEKIT_INT(bri); }

void servo_set(homekit_value_t value) {
    if (value.format != homekit_format_int) {
        printf("Invalid bri-value format: %d\n", value.format);
        return;
    }
    bri = value.int_value;
    //servoSET();
    stepperMotorSET();
}


homekit_accessory_t *accessories[] = {
    HOMEKIT_ACCESSORY(.id=1, .category=homekit_accessory_category_lightbulb, .services=(homekit_service_t*[]){
        HOMEKIT_SERVICE(ACCESSORY_INFORMATION, .characteristics=(homekit_characteristic_t*[]){
            HOMEKIT_CHARACTERISTIC(NAME, "Servo extension"),
            HOMEKIT_CHARACTERISTIC(MANUFACTURER, "Consultware"),
            HOMEKIT_CHARACTERISTIC(SERIAL_NUMBER, "037A2BA"),
            HOMEKIT_CHARACTERISTIC(MODEL, "MyServo"),
            HOMEKIT_CHARACTERISTIC(FIRMWARE_REVISION, "0.1"),
            HOMEKIT_CHARACTERISTIC(IDENTIFY, led_identify),
            NULL
        }),
        
        HOMEKIT_SERVICE(LIGHTBULB, .primary=true, .characteristics=(homekit_characteristic_t*[]){
            HOMEKIT_CHARACTERISTIC(NAME, "Sample LED"),
            &led_on,
            HOMEKIT_CHARACTERISTIC(BRIGHTNESS, 100, .getter=servo_get, .setter=servo_set),
            NULL
        }),
        NULL
    }),
    NULL
};

homekit_server_config_t config = {
    .accessories = accessories,
    .password = "111-11-112"
};

void on_wifi_ready() {
    homekit_server_init(&config);
}
void servo_init() {
    printf("light_init:\n");
    on=false;
    bri=100;
    printf("on = false  bri = 100 %%\n");
    pwm_set_freq(50);
    printf("PWMpwm_set_freq = 50 Hz  pwm_set_duty = 0 = 0%%\n");
    pwm_set_duty(28888);
    pwm_start();
    servoSET();
}
void user_init(void) {
    uart_set_baud(0, 115200);

    wifi_config_init("cw-servo", NULL, on_wifi_ready);
    led_init();
    //servo_init();
    // the bri variable controls the angle of the servo
    // 50 when servo is used or 0 when stepper instead
     //bri = 50;
    // servoSET();
   //section to initialize the pins for the stepper motor
   bri = 0;
   stepper_init();
   /* int grados = 10;
   direction = true;
   while(1){
      bri = bri + 10;
      if( bri > 180 ){
         bri = 0;
      }
      //servoSET();
      move_stepper();
      vTaskDelay(10 / portTICK_PERIOD_MS);
   }*/
}

